# README #

This is a sample android application written for a coding assignment. It uses the timekeeping cloud API (https://github.com/jonathanpike/timekeeping-api) to track a user's time.

### Technical Choices ###

* For this app I used Retrofit library for type safe http requests. This was done to quickly build the app and keep the code as simple as possible. 
* I used the gson library to parse response from the timekeeping API.

### What's left? ###

* This app does not fully implement data caching. With more time it would have included caching of data for when internet connectivity is bad.
* Testing was not fully implemented.