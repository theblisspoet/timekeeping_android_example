package com.example.timekeeping;

import android.app.Application;
import android.content.Intent;

import com.example.timekeeping.service.TCService;

/**
 * Created by ahmedsaad on 2016-12-02
 */
public class TimeKeeping extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //Sync the database when application starts.
        Intent intent = new Intent(this, TCService.class);
        startService(intent);
    }
}
