package com.example.timekeeping.adapters;

import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.timekeeping.R;
import com.example.timekeeping.model.TimeCard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ahmedsaad on 2016-12-02.
 * TODO: Implement data caching and pull data from local storage
 */

public class TimeCardAdapter extends RecyclerView.Adapter<TimeCardAdapter.ViewHolder> {
    private Cursor mCursor;
    private ArrayList<TimeCard> mDataset;
    final private adapterOnClickHandler mClickHandler;
    int selected_position = -1;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {
        // each data item is just a string in this case
        public TextView mDateTextView;
        public TextView mHoursTextView;
        public ViewHolder(View v) {
            super(v);

            mDateTextView = (TextView) v.findViewById(R.id.date_text_view);
            mHoursTextView = (TextView) v.findViewById(R.id.hours_text_view);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickHandler.onClick(this, v);
        }

        @Override
        public boolean onLongClick(View view) {
            notifyItemChanged(selected_position);
            if (selected_position != getLayoutPosition()) {
                selected_position = getLayoutPosition();
                notifyItemChanged(selected_position);
            } else {
                selected_position = -1;
            }
            return false;
        }
    }

    public void addAll(List<TimeCard> timeCards) {
        mDataset.addAll(timeCards);

        Collections.sort(mDataset, new Comparator<TimeCard>() {
            @Override
            public int compare(TimeCard t1, TimeCard t2) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
                return format.format(t2.getOccurrence())
                        .compareTo(format.format(t1.getOccurrence()));
            }
        });
        notifyDataSetChanged();
    }

    public void removeAll() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    public void add(int position, TimeCard item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(TimeCard item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    /* Use this function to retrieve the time card if a time card for the selected date is already made*/
    public TimeCard getTimeCard(Date date) {

        for (int i = 0; i < mDataset.size(); i++){
            TimeCard timeCard = mDataset.get(i);

            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.US);
            if (fmt.format(timeCard.getOccurrence()).equals(fmt.format(date))) {
                return timeCard;
            }
        }
        return null;
    }

    public TimeCard getTimeCard(int position) {
        return mDataset.get(position);
    }

    public interface adapterOnClickHandler {
        void onClick(ViewHolder vh, View v);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TimeCardAdapter(ArrayList<TimeCard> myDataset, adapterOnClickHandler dh) {
        mDataset = myDataset;
        mClickHandler = dh;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TimeCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_time_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        if(selected_position == position){
            // Here I am just highlighting the background
            holder.itemView.setBackgroundColor(Color.GREEN);
        }else{
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        TimeCard timeCard = mDataset.get(position);

        DateFormat newFormat = new SimpleDateFormat("MMMM dd, ''yy", Locale.getDefault());
        holder.mDateTextView.setText(newFormat.format(timeCard.getOccurrence()));

        if (timeCard.getTotalWorkedHours() != null)
            holder.mHoursTextView.setText(String.format(Locale.US, "%.2fh", timeCard.getTotalWorkedHours()));

        else if (timeCard.getTimeEntries().size() == 1) {
            holder.mHoursTextView.setText(R.string.punched_in);
        } else {

            holder.mHoursTextView.setText(null);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swapCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
    }

    public Cursor getCursor() {
        return mCursor;
    }
}