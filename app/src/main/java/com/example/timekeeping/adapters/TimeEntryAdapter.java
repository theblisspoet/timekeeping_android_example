package com.example.timekeeping.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.timekeeping.R;
import com.example.timekeeping.model.TimeEntry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public class TimeEntryAdapter extends RecyclerView.Adapter<TimeEntryAdapter.ViewHolder> {
    private ArrayList<TimeEntry> mDataset;
    int selected_position = -1;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mDateTextView;
        public TextView mHoursTextView;
        public ViewHolder(View v) {
            super(v);

            mDateTextView = (TextView) v.findViewById(R.id.date_text_view);
            mHoursTextView = (TextView) v.findViewById(R.id.hours_text_view);
        }
    }

    public void addAll(List<TimeEntry> timeEntries) {
        mDataset.addAll(timeEntries);

        Collections.sort(mDataset, new Comparator<TimeEntry>() {
            @Override
            public int compare(TimeEntry t1, TimeEntry t2) {
                return t1.getTime()
                        .compareTo(t2.getTime());
            }
        });
        notifyDataSetChanged();
    }

    public void removeAll() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    public void add(int position, TimeEntry item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(TimeEntry item) {
        int position = mDataset.indexOf(item);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TimeEntryAdapter(ArrayList<TimeEntry> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TimeEntryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_time_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        TimeEntry timeEntry = mDataset.get(position);

        DateFormat newFormat = new SimpleDateFormat("hh:mm a", Locale.US);

        if (timeEntry.getTime() != null)
            holder.mHoursTextView.setText(newFormat.format(timeEntry.getTime()));
        else
            holder.mHoursTextView.setText(null);

        if (position == 0) {
            holder.mDateTextView.setText(R.string.in);
        } else {
            holder.mDateTextView.setText(R.string.out);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}