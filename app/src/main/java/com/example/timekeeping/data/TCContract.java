package com.example.timekeeping.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ahmedsaad on 2016-12-02
 */
public class TCContract {
    public static final String CONTENT_AUTHORITY = "com.example.timekeeping";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://com.example.timekeeping/timecards/ is a valid path for
    // looking at services data.
    public static final String PATH_TIMECARD = "timecards";
    public static final String PATH_TIME_ENTRY = "time_entries";

    /* Inner class that defines the table contents of the timecards table */
    public static final class TimecardEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TIMECARD).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                CONTENT_AUTHORITY + "/" + PATH_TIMECARD;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                CONTENT_AUTHORITY + "/" + PATH_TIMECARD;

        // Table name
        public static final String TABLE_NAME = "timecards";

        public static final String COLUMN_OCCURRENCE = "occurrence";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_TOTAL_WORKED_HOURS = "total_worked_hours";
        public static final String COLUMN_EXCEPTION = "exception";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_UPDATED_AT = "updated_at";

        public static Uri buildTimecardsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    /* Inner class that defines the table contents of the time entries table */
    public static final class TimeEntriesEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TIME_ENTRY).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                CONTENT_AUTHORITY + "/" + PATH_TIME_ENTRY;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                CONTENT_AUTHORITY + "/" + PATH_TIME_ENTRY;

        // Table name
        public static final String TABLE_NAME = "time_entries";

        public static final String COLUMN_TIME = "time";
        public static final String COLUMN_TIMECARD_ID = "timecard_id";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_UPDATED_AT = "updated_at";

        public static Uri buildTimeEntriesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }
}
