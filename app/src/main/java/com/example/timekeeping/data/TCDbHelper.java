package com.example.timekeeping.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ahmedsaad on 2016-12-02
 */
public class TCDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "timekeeping.db";
    private Context mContext;

    public TCDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_TIMECARD_TABLE = "CREATE TABLE " + TCContract.TimecardEntry.TABLE_NAME + " (" +
                TCContract.TimecardEntry._ID + " INTEGER PRIMARY KEY, " +
                TCContract.TimecardEntry.COLUMN_OCCURRENCE + " TEXT, " +
                TCContract.TimecardEntry.COLUMN_USERNAME + " TEXT, " +
                TCContract.TimecardEntry.COLUMN_TOTAL_WORKED_HOURS + " REAL, " +
                TCContract.TimecardEntry.COLUMN_EXCEPTION + " TEXT, " +
                TCContract.TimecardEntry.COLUMN_CREATED_AT + " TEXT, " +
                TCContract.TimecardEntry.COLUMN_UPDATED_AT + " TEXT, " +

                // To assure the application have just one service entry per id
                // it's created a UNIQUE constraint with REPLACE strategy
                " UNIQUE (" + TCContract.TimecardEntry._ID + ") ON CONFLICT REPLACE);";

        final String SQL_CREATE_TIMEENTRY_TABLE = "CREATE TABLE " + TCContract.TimeEntriesEntry.TABLE_NAME + " (" +
                TCContract.TimeEntriesEntry._ID + " INTEGER PRIMARY KEY, " +
                TCContract.TimeEntriesEntry.COLUMN_TIME + " TEXT, " +
                TCContract.TimeEntriesEntry.COLUMN_TIMECARD_ID + " INTEGER, " +
                TCContract.TimeEntriesEntry.COLUMN_CREATED_AT + " TEXT, " +
                TCContract.TimeEntriesEntry.COLUMN_UPDATED_AT + " TEXT, " +

                // Set up the service column as a foreign key to service table.
                " FOREIGN KEY (" + TCContract.TimeEntriesEntry.COLUMN_TIMECARD_ID + ") REFERENCES " +
                TCContract.TimecardEntry.TABLE_NAME + " (" + TCContract.TimecardEntry._ID + "), " +

                // To assure the application have just one job entry per id
                // it's created a UNIQUE constraint with REPLACE strategy
                " UNIQUE (" + TCContract.TimeEntriesEntry._ID + ") ON CONFLICT REPLACE);";

        sqLiteDatabase.execSQL(SQL_CREATE_TIMECARD_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_TIMEENTRY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.
        // If you want to update the schema without wiping data, commenting out the next 4 lines
        // should be your top priority before modifying this method.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TCContract.TimecardEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TCContract.TimeEntriesEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

}
