package com.example.timekeeping.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Created by ahmedsaad on 2016-12-02
 */
public class TCProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private TCDbHelper mOpenHelper;

    static final int TIMECARD = 100;
    static final int TIMECARD_WITH_ID = 101;
    static final int TIMEENTRY = 300;
    static final int TIMEENTRY_WITH_ID = 301;

    /*
        This UriMatcher will match each URI to the SERVICE and JOB integer constants defined above.
     */
    static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = TCContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, TCContract.PATH_TIMECARD, TIMECARD);
        matcher.addURI(authority, TCContract.PATH_TIMECARD + "/#", TIMECARD_WITH_ID);
        matcher.addURI(authority, TCContract.PATH_TIME_ENTRY, TIMEENTRY);
        matcher.addURI(authority, TCContract.PATH_TIME_ENTRY + "/#", TIMEENTRY_WITH_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new TCDbHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case TIMECARD:
                return TCContract.TimecardEntry.CONTENT_TYPE;
            case TIMECARD_WITH_ID:
                return TCContract.TimecardEntry.CONTENT_ITEM_TYPE;
            case TIMEENTRY:
                return TCContract.TimeEntriesEntry.CONTENT_TYPE;
            case TIMEENTRY_WITH_ID:
                return TCContract.TimeEntriesEntry.CONTENT_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {
            // "timecards"
            case TIMECARD: {
                retCursor = mOpenHelper.getReadableDatabase().query(TCContract.TimecardEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }
            // "timecards/#"
            case TIMECARD_WITH_ID: {
                retCursor = mOpenHelper.getReadableDatabase().query(TCContract.TimecardEntry.TABLE_NAME,
                        projection,
                        TCContract.TimecardEntry._ID + " = ?",
                        new String[]{TCContract.TimecardEntry.getIdFromUri(uri)},
                        null,
                        null,
                        sortOrder);

                break;
            }
            // "time_entries"
            case TIMEENTRY: {
                retCursor = mOpenHelper.getReadableDatabase().query(TCContract.TimeEntriesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            // "time_entries/#"
            case TIMEENTRY_WITH_ID: {
                retCursor = mOpenHelper.getReadableDatabase().query(TCContract.TimeEntriesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case TIMECARD: {
                long _id = db.insertWithOnConflict(TCContract.TimecardEntry.TABLE_NAME,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = TCContract.TimecardEntry.buildTimecardsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case TIMEENTRY: {
                long _id = db.insertWithOnConflict(TCContract.TimeEntriesEntry.TABLE_NAME,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                if ( _id > 0 )
                    returnUri = TCContract.TimeEntriesEntry.buildTimeEntriesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Start by getting a writable database
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        // Use the uriMatcher to match the SERVICE and JOB URI's we are going to
        // handle.  If it doesn't match these, throw an UnsupportedOperationException.
        final int match = sUriMatcher.match(uri);

        int rowsDeleted = 0;

        // Delete all rows if selection is null.
        if ( selection == null) selection = "1";

        switch (match) {
            case TIMECARD: {
                rowsDeleted = db.delete(TCContract.TimecardEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            case TIMEENTRY: {
                rowsDeleted = db.delete(TCContract.TimeEntriesEntry.TABLE_NAME, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // A null value deletes all rows.  In my implementation of this, I only notified
        // the uri listeners (using the content resolver) if the rowsDeleted != 0 or the selection
        // is null.
        // Oh, and you should notify the listeners here.
        if (rowsDeleted != 0)
            getContext().getContentResolver().notifyChange(uri, null);

        // Return the actual rows deleted
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // Start by getting a writable database
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        // Use the uriMatcher to match the SERVICE and JOB URI's we are going to
        // handle.  If it doesn't match these, throw an UnsupportedOperationException.
        final int match = sUriMatcher.match(uri);

        int rowsUpdated = 0;

        switch (match) {
            case TIMECARD: {
                rowsUpdated = db.update(TCContract.TimecardEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            case TIMEENTRY: {
                rowsUpdated = db.update(TCContract.TimeEntriesEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // In my implementation of this, I only notified
        // the uri listeners (using the content resolver) if the rowsUpdated != 0 or the selection
        // is null.
        // Oh, and you should notify the listeners here.
        if (rowsUpdated != 0)
            getContext().getContentResolver().notifyChange(uri, null);

        // Return the actual rows updated
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);

        int returnCount = 0;

        switch (match) {
            case TIMECARD: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(TCContract.TimecardEntry.TABLE_NAME,
                                null,
                                value,
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            case TIMEENTRY: {
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(TCContract.TimeEntriesEntry.TABLE_NAME,
                                null,
                                value,
                                SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            default:
                return super.bulkInsert(uri, values);
        }

    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}
