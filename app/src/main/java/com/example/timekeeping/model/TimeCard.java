package com.example.timekeeping.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public class TimeCard {
    private Integer id;
    private Date occurrence;
    private String username;
    private Float total_worked_hours;
    private Boolean exception;
    private Date created_at;
    private Date updated_at;
    private List<TimeEntry> time_entries = new ArrayList<TimeEntry>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The occurrence
     */
    public Date getOccurrence() {
        return occurrence;
    }

    /**
     *
     * @param occurrence
     * The occurrence
     */
    public void setOccurrence(Date occurrence) {
        this.occurrence = occurrence;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The totalWorkedHours
     */
    public Float getTotalWorkedHours() {
        return total_worked_hours;
    }

    /**
     *
     * @param totalWorkedHours
     * The total_worked_hours
     */
    public void setTotalWorkedHours(Float totalWorkedHours) {
        this.total_worked_hours = totalWorkedHours;
    }

    /**
     *
     * @return
     * The exception
     */
    public Boolean getException() {
        return exception;
    }

    /**
     *
     * @param exception
     * The exception
     */
    public void setException(Boolean exception) {
        this.exception = exception;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public Date getCreatedAt() {
        return created_at;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(Date createdAt) {
        this.created_at = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public Date getUpdatedAt() {
        return updated_at;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updated_at = updatedAt;
    }

    /**
     *
     * @return
     * The timeEntries
     */
    public List<TimeEntry> getTimeEntries() {
        return time_entries;
    }

    /**
     *
     * @param timeEntries
     * The time_entries
     */
    public void setTimeEntries(List<TimeEntry> timeEntries) {
        this.time_entries = timeEntries;
    }

}
