package com.example.timekeeping.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public class TimeCardWrapper {
    private Integer total;
    private List<TimeCard> timecards = new ArrayList<TimeCard>();

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The timecards
     */
    public List<TimeCard> getTimecards() {
        return timecards;
    }

    /**
     *
     * @param timecards
     * The timecards
     */
    public void setTimecards(List<TimeCard> timecards) {
        this.timecards = timecards;
    }
}
