package com.example.timekeeping.model;

import java.util.Date;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public class TimeEntry {
    private Integer id;
    private Date time;
    private Integer timecard_id;
    private Date created_at;
    private Date updated_at;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The time
     */
    public Date getTime() {
        return time;
    }

    /**
     *
     * @param time
     * The time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     *
     * @return
     * The timecardId
     */
    public Integer getTimecardId() {
        return timecard_id;
    }

    /**
     *
     * @param timecardId
     * The timecard_id
     */
    public void setTimecardId(Integer timecardId) {
        this.timecard_id = timecardId;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public Date getCreatedAt() {
        return created_at;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(Date createdAt) {
        this.created_at = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public Date getUpdatedAt() {
        return updated_at;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updated_at = updatedAt;
    }
}
