package com.example.timekeeping.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public class TimeEntryWrapper {
    private Integer total;
    private List<TimeEntry> timeentries = new ArrayList<TimeEntry>();

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The timeentries
     */
    public List<TimeEntry> getTimeentries() {
        return timeentries;
    }

    /**
     *
     * @param timeentries
     * The timecards
     */
    public void setTimeentries(List<TimeEntry> timeentries) {
        this.timeentries = timeentries;
    }
}
