package com.example.timekeeping.model;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public interface TimeKeepingService {
    /*Time Cards API Services*/
    @GET("timecards")
    Call <TimeCardWrapper> listTimeCards();

    @GET("timecards/{id}")
    Call <TimeCard>  getTimeCard(@Path("id") int id);

    @FormUrlEncoded
    @POST("timecards")
    Call <TimeCard> createTimeCard(@Field("timecard[username]") String username,
                                   @Field("timecard[occurrence]") String occurrence);

    @FormUrlEncoded
    @PUT("timecards/{id}")
    Call <TimeCard> updateTimeCard(@Path("id") int id, @Field("timecard[username]") String username,
                                   @Field("timecard[occurrence]") String occurrence);

    @DELETE("timecards/{id}")
    Call<Void> deleteTimeCard(@Path("id") int id);

    /*Time Entries API Services*/
    @GET("time_entries")
    Call <TimeEntryWrapper> listTimeEntries();

    @GET("time_entries/{id}")
    Call <TimeEntry>  getTimeEntry(@Path("id") int id);

    @FormUrlEncoded
    @POST("time_entries")
    Call <TimeCard> createTimeEntry(@Field("time_entry[time]") String time,
                                   @Field("time_entry[timecard_id]") int timecard_id);

    @FormUrlEncoded
    @PUT("time_entries/{id}")
    Call <TimeCard> updateTimeEntry(@Path("id") int id, @Field("time_entry[time]") String time,
                                    @Field("time_entry[timecard_id]") int timecard_id);

    @DELETE("time_entries/{id}")
    Call<Void> deleteTimeEntry(@Path("id") int id);

}
