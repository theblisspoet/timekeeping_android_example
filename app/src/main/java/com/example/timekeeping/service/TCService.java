package com.example.timekeeping.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.example.timekeeping.data.TCContract;
import com.example.timekeeping.model.TimeCard;
import com.example.timekeeping.model.TimeCardWrapper;
import com.example.timekeeping.model.TimeEntry;
import com.example.timekeeping.model.TimeKeepingService;
import com.example.timekeeping.utils.CleanGsonConverter;
import com.example.timekeeping.utils.Constants;
import com.example.timekeeping.utils.LogHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;

import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ahmedsaad on 2016-12-02
 */
public class TCService extends IntentService {
    private static final String LOG_TAG = TCService.class.getSimpleName();

    public TCService() {
        super("TCService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(CleanGsonConverter.create())
                .build();

        TimeKeepingService apiService = retrofit.create(TimeKeepingService.class);

        try {
            Response<TimeCardWrapper> response = apiService.listTimeCards().execute();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);

            Vector<ContentValues> timeCardVector = new Vector<>(response.body().getTimecards().size());
            Vector<ContentValues> timeEntryVector = new Vector<>();
            for (TimeCard timeCard: response.body().getTimecards()) {
                ContentValues timeCardValues = new ContentValues();
                timeCardValues.put(TCContract.TimecardEntry._ID, timeCard.getId());
                timeCardValues.put(TCContract.TimecardEntry.COLUMN_OCCURRENCE, format.format(timeCard.getOccurrence()));
                timeCardValues.put(TCContract.TimecardEntry.COLUMN_TOTAL_WORKED_HOURS, timeCard.getTotalWorkedHours());
                timeCardValues.put(TCContract.TimecardEntry.COLUMN_EXCEPTION, timeCard.getException());
                timeCardValues.put(TCContract.TimecardEntry.COLUMN_CREATED_AT, format.format(timeCard.getCreatedAt()));
                timeCardValues.put(TCContract.TimecardEntry.COLUMN_UPDATED_AT, format.format(timeCard.getUpdatedAt()));

                for (TimeEntry timeEntry: timeCard.getTimeEntries()) {
                    ContentValues timeEntryValues = new ContentValues();
                    timeCardValues.put(TCContract.TimeEntriesEntry._ID, timeEntry.getId());
                    timeCardValues.put(TCContract.TimeEntriesEntry.COLUMN_TIME, format.format(timeEntry.getTime()));
                    timeCardValues.put(TCContract.TimeEntriesEntry.COLUMN_TIMECARD_ID, timeEntry.getTimecardId());
                    timeCardValues.put(TCContract.TimeEntriesEntry.COLUMN_CREATED_AT, format.format(timeEntry.getCreatedAt()));
                    timeCardValues.put(TCContract.TimeEntriesEntry.COLUMN_UPDATED_AT, format.format(timeEntry.getUpdatedAt()));

                    timeEntryVector.add(timeEntryValues);
                }

                timeCardVector.add(timeCardValues);
            }

            int inserted = 0;
            // add to database
            if ( timeCardVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[timeCardVector.size()];
                timeCardVector.toArray(cvArray);
                inserted = this.getContentResolver().bulkInsert(
                        TCContract.TimecardEntry.CONTENT_URI, cvArray);
            }

            LogHelper.d(LOG_TAG, "Fetch Time Cards Completed. " + inserted + " Inserted Time Cards ");

            inserted = 0;
            // add to database
            if ( timeEntryVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[timeEntryVector.size()];
                timeEntryVector.toArray(cvArray);
                inserted = this.getContentResolver().bulkInsert(
                        TCContract.TimeEntriesEntry.CONTENT_URI, cvArray);
            }

            LogHelper.d(LOG_TAG, "Fetch Time Entries Completed. " + inserted + " Inserted Time Entries ");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Send a localbroadcast to tell app sync is complete
        Intent response = new Intent("sync_complete");
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(response);
    }

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public TCService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TCService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    static public class AlarmReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent sendIntent = new Intent(context, TCService.class);
            context.startService(sendIntent);
        }
    }
}
