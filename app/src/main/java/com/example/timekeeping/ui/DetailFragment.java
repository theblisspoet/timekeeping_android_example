package com.example.timekeeping.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.timekeeping.R;
import com.example.timekeeping.adapters.TimeEntryAdapter;
import com.example.timekeeping.model.TimeCard;
import com.example.timekeeping.model.TimeEntry;
import com.example.timekeeping.utils.Constants;
import com.example.timekeeping.utils.TimeKeepingAPI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmedsaad on 2016-12-05.
 */

public class DetailFragment extends Fragment implements TimeKeepingAPI.TimeKeepingAPIListener {
    private TextView mHeaderView;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private TimeEntryAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private View rootView;

    private TimeKeepingAPI mApiClient;

    private int mTimeCardId;

    public DetailFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        if (getActivity().getIntent().hasExtra(Constants.TIMECARD_ID)) {
            mTimeCardId = getActivity().getIntent().getIntExtra(Constants.TIMECARD_ID, -1);

            if (mTimeCardId == -1)
                getActivity().finish();
        } else {
            getActivity().finish();
        }

        rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        mHeaderView = (TextView) rootView.findViewById(R.id.header_text_view);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new TimeEntryAdapter(new ArrayList<TimeEntry>());

        mRecyclerView.setAdapter(mAdapter);

        mApiClient = new TimeKeepingAPI(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        /*Load Data on resume*/
        refreshData();
    }

    void refreshData () {
        mSwipeRefreshLayout.setRefreshing(true);

        mApiClient.getApiService().getTimeCard(mTimeCardId).enqueue(new Callback<TimeCard>() {
            @Override
            public void onResponse(Call<TimeCard> call, Response<TimeCard> response) {
                DateFormat newFormat = new SimpleDateFormat("MMMM dd, ''yy", Locale.getDefault());
                mHeaderView.setText(newFormat.format(response.body().getOccurrence()));

                mAdapter.removeAll();
                mAdapter.addAll(response.body().getTimeEntries());
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<TimeCard> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onFailure(String message) {

    }
}
