package com.example.timekeeping.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.timekeeping.R;
import com.example.timekeeping.utils.Constants;

public class MainActivity extends AppCompatActivity implements MainFragment.MainFragmentListener {

    private static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            // Create the fragment and add it to the activity
            // using a fragment transaction.
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, new MainFragment(),
                            Constants.MAIN_FRAGMENT).commit();
        }
    }

    // Replace Main Fragment with animation
    protected void replaceMainFragment() {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.right_enter, R.anim.left_exit)
                .replace(R.id.main_container, new DetailFragment(),
                        Constants.DETAIL_FRAGMENT).commit();
    }

    // Replace Main Fragment with animation
    protected void returnMainFragment() {
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.left_enter, R.anim.right_exit)
                .replace(R.id.main_container, new MainFragment(),
                        Constants.MAIN_FRAGMENT).commit();
    }

    @Override
    public void onClick(int id) {
        getIntent().putExtra(Constants.TIMECARD_ID, id);
        replaceMainFragment();
    }

    @Override
    public void onBackPressed() {

        // Find the tag of detail fragment
        Fragment DetailFragment = fragmentManager
                .findFragmentByTag(Constants.DETAIL_FRAGMENT);

        if (DetailFragment != null)
            returnMainFragment();
    }
}
