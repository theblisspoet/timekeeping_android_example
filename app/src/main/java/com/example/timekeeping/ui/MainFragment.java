package com.example.timekeeping.ui;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.timekeeping.R;
import com.example.timekeeping.adapters.TimeCardAdapter;
import com.example.timekeeping.data.TCContract;
import com.example.timekeeping.model.TimeCard;
import com.example.timekeeping.model.TimeCardWrapper;
import com.example.timekeeping.utils.TimeKeepingAPI;
import com.example.timekeeping.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmedsaad on 2016-12-02.
 * TODO: Implement data caching and pull data from local storage
 */

public class MainFragment extends Fragment implements View.OnClickListener, TimeKeepingAPI.TimeKeepingAPIListener,
        LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private TimeCardAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressDialogFragment progressFragment;

    private View rootView;
    private TimeKeepingAPI mApiClient;

    private MainFragmentListener mMainFragmentListener;

    private static final int TIMECARD_LOADER = 0;

    public static final String [] TIMECARD_COLUMNS = {
            TCContract.TimecardEntry.TABLE_NAME + "." + TCContract.TimecardEntry._ID,
            TCContract.TimecardEntry.TABLE_NAME + "." + TCContract.TimecardEntry.COLUMN_OCCURRENCE,
            TCContract.TimecardEntry.TABLE_NAME + "." + TCContract.TimecardEntry.COLUMN_TOTAL_WORKED_HOURS
    };

    static final int COL_TIMECARD_ID = 0;
    static final int COL_OCCURRENCE = 1;
    static final int COL_TOTAL_WORKED_HOURS = 2;

    public MainFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        Button punchInButton = (Button) rootView.findViewById(R.id.punch_in_button);
        punchInButton.setOnClickListener(this);

        Button punchOutButton = (Button) rootView.findViewById(R.id.punch_out_button);
        punchOutButton.setOnClickListener(this);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new TimeCardAdapter(new ArrayList<TimeCard>(), new TimeCardAdapter.adapterOnClickHandler() {
            @Override
            public void onClick(TimeCardAdapter.ViewHolder vh, View v) {
                TimeCard timeCard = mAdapter.getTimeCard(vh.getAdapterPosition());
                mMainFragmentListener.onClick(timeCard.getId());
            }
        });

        mRecyclerView.setAdapter(mAdapter);

        mApiClient = new TimeKeepingAPI(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        progressFragment = ProgressDialogFragment.newInstance(R.string.loading);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        /*Load Data on resume*/
        refreshData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mMainFragmentListener = (MainFragmentListener) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mMainFragmentListener = null;
    }

    void refreshData () {
        mSwipeRefreshLayout.setRefreshing(true);

        mApiClient.getApiService().listTimeCards().enqueue(new Callback<TimeCardWrapper>() {
            @Override
            public void onResponse(Call<TimeCardWrapper> call, Response<TimeCardWrapper> response) {

                mAdapter.removeAll();
                mAdapter.addAll(response.body().getTimecards());
                mSwipeRefreshLayout.setRefreshing(false);

                if (progressFragment != null && progressFragment.isAdded())
                    progressFragment.dismissAllowingStateLoss();
            }

            @Override
            public void onFailure(Call<TimeCardWrapper> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                if (progressFragment != null && progressFragment.isAdded())
                    progressFragment.dismissAllowingStateLoss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View view) {
        progressFragment.show(getActivity().getSupportFragmentManager(), "progress");
        TimeCard timeCard = mAdapter.getTimeCard(new Date());

        switch (view.getId()) {
            case R.id.punch_in_button:
                mApiClient.punchIn(timeCard);
                break;
            case R.id.punch_out_button:
                mApiClient.punchOut(timeCard);
                break;
            default:
                break;
        }
    }

    @Override
    public void onComplete() {
        refreshData();
    }

    @Override
    public void onFailure(String message) {
        Utils.showMessage(rootView, message, Color.RED);

        if (progressFragment != null && progressFragment.isAdded())
            progressFragment.dismissAllowingStateLoss();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case TIMECARD_LOADER: {
                String sortOrder = TCContract.TimecardEntry.COLUMN_OCCURRENCE + " DESC";

                return new CursorLoader(getActivity(),
                        TCContract.TimecardEntry.CONTENT_URI,
                        TIMECARD_COLUMNS,
                        null, null,
                        sortOrder);
            }
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case TIMECARD_LOADER: {
                mAdapter.swapCursor(data);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case TIMECARD_LOADER: {
                mAdapter.swapCursor(null);
                break;
            }
            default:
                return;
        }
    }

    public interface MainFragmentListener {
        /**
         * Callback for when a date has been clicked.
         */
        void onClick(int id);
    }
}
