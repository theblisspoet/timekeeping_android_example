package com.example.timekeeping.utils;

/**
 * Created by ahmedsaad on 2016-12-02.
 */

public class Constants {
    public static final String BASE_URL = "https://timekeeping-api.herokuapp.com/api/v1/";
    public static final String USER_NAME = "userA";

    public static final String MAIN_FRAGMENT = "com.example.timekeeping.MAIN_FRAGMENT";
    public static final String DETAIL_FRAGMENT = "com.example.timekeeping.DETAIL_FRAGMENT";
    public static final String TIMECARD_ID = "com.example.timekeeping.TIMECARD_ID";
}
