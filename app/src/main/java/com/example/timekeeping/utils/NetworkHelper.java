package com.example.timekeeping.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Generic reusable network methods.
 */
public class NetworkHelper {
    /**
     * @param context to use to check for network connectivity.
     * @return true if connected, false otherwise.
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
            context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static String httpCall(String uri, String method, String body) {
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(uri);

            //Create the request and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(method);

            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");

            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream (
                    urlConnection.getOutputStream ());
            wr.writeBytes(body);
            wr.flush();
            wr.close ();

            urlConnection.connect();

            if (urlConnection.getResponseCode() > 299 || urlConnection.getResponseCode() < 200) {
                throw new Exception("Error calling " + uri + " ["+urlConnection.getResponseCode()+"] - "
                        + urlConnection.getResponseMessage());

            }
            else {
                //Read Input Stream into String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                if (inputStream == null)
                    return null;

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // New line not needed. Used for debug
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0)
                    return null;

                String responseString = buffer.toString();

                inputStream.close();
                reader.close();

                return responseString;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }

        return null;
    }
}
