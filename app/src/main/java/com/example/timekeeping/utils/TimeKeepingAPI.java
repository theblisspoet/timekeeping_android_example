package com.example.timekeeping.utils;

import android.support.v4.app.Fragment;

import com.example.timekeeping.model.TimeCard;
import com.example.timekeeping.model.TimeEntry;
import com.example.timekeeping.model.TimeKeepingService;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ahmedsaad on 2016-12-05.
 */

public class TimeKeepingAPI {

    private TimeKeepingService apiService;
    private TimeKeepingAPIListener mTimeKeepingAPIListener;

    public TimeKeepingAPI(Fragment fragment)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(CleanGsonConverter.create())
                .build();

        apiService = retrofit.create(TimeKeepingService.class);

        mTimeKeepingAPIListener = (TimeKeepingAPIListener) fragment;
    }

    public TimeKeepingService getApiService() {
        return apiService;
    }

    public void punchIn (TimeCard timeCard) {
        if (timeCard == null) {
            apiService.createTimeCard(Constants.USER_NAME, ISO8601.now())
                    .enqueue(new Callback<TimeCard>() {
                        @Override
                        public void onResponse(Call<TimeCard> call, Response<TimeCard> response) {
                            punchIn(response.body());
                        }

                        @Override
                        public void onFailure(Call<TimeCard> call, Throwable t) {
                            mTimeKeepingAPIListener.onFailure(t.getMessage());
                        }
                    });
        } else {
            if (timeCard.getTimeEntries().size() == 2) {
                // If 2 time entries exist remove one and update the other.
                // TODO: The api seems to be broken when it comes to deleting time entries. For now
                // delete the entire time card and recreate.
                apiService.deleteTimeCard(timeCard.getId()).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        punchIn(null);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        mTimeKeepingAPIListener.onFailure(t.getMessage());
                    }
                });
            } else if (timeCard.getTimeEntries().size() == 1) {
                // If only one Time Entry already exists; update that time.
                TimeEntry timeEntry = timeCard.getTimeEntries().get(0);

                apiService.updateTimeEntry(timeEntry.getId(), ISO8601.now(), timeCard.getId())
                        .enqueue(new Callback<TimeCard>() {
                            @Override
                            public void onResponse(Call<TimeCard> call, Response<TimeCard> response) {
                                mTimeKeepingAPIListener.onComplete();
                            }

                            @Override
                            public void onFailure(Call<TimeCard> call, Throwable t) {
                                mTimeKeepingAPIListener.onFailure(t.getMessage());
                            }
                        });
            } else {
                apiService.createTimeEntry(ISO8601.now(), timeCard.getId()).enqueue(new Callback<TimeCard>() {
                    @Override
                    public void onResponse(Call<TimeCard> call, Response<TimeCard> response) {
                        mTimeKeepingAPIListener.onComplete();
                    }

                    @Override
                    public void onFailure(Call<TimeCard> call, Throwable t) {
                        mTimeKeepingAPIListener.onFailure(t.getMessage());
                    }
                });
            }
        }
    }

    public void punchOut(TimeCard timeCard) {
        if (timeCard == null) {
            mTimeKeepingAPIListener.onFailure("Please first punch in.");
        } else {
            if (timeCard.getTimeEntries().size() == 2) {
                /// If two Time Entry already exists; update the latest time.
                TimeEntry t1 = timeCard.getTimeEntries().get(0);
                TimeEntry t2 = timeCard.getTimeEntries().get(1);

                apiService.updateTimeEntry(t1.getTime().before(t2.getTime()) ? t2.getId() : t2.getId(),
                        ISO8601.now(), timeCard.getId())
                        .enqueue(new Callback<TimeCard>() {
                            @Override
                            public void onResponse(Call<TimeCard> call, Response<TimeCard> response) {
                                mTimeKeepingAPIListener.onComplete();
                            }

                            @Override
                            public void onFailure(Call<TimeCard> call, Throwable t) {
                                mTimeKeepingAPIListener.onFailure(t.getMessage());
                            }
                        });
            } else if (timeCard.getTimeEntries().size() == 1) {
                apiService.createTimeEntry(ISO8601.now(), timeCard.getId()).enqueue(new Callback<TimeCard>() {
                    @Override
                    public void onResponse(Call<TimeCard> call, Response<TimeCard> response) {
                        mTimeKeepingAPIListener.onComplete();
                    }

                    @Override
                    public void onFailure(Call<TimeCard> call, Throwable t) {
                        mTimeKeepingAPIListener.onFailure(t.getMessage());
                    }
                });
            } else {
                mTimeKeepingAPIListener.onFailure("Please first punch in.");
            }
        }
    }

    public interface TimeKeepingAPIListener {
        /**
         * Callback for when a service has been competed.
         */
        void onComplete();

        /**
         * Callback for when a service has failed.
         */
        void onFailure(String message);
    }

}
