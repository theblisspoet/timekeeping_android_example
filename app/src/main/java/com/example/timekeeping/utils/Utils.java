package com.example.timekeeping.utils;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by ahmedsaad on 2016-12-05.
 */

public class Utils {
    // Used to change the color of the text
    public static void showMessage(View container, String msg, int color) {
        Snackbar snack = Snackbar.make(container, msg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(color);
        snack.show();
    }
}
